const { useState } = React;
/**
 * Sidebar functional component for handling search and displaying results.
 * @component
 * @param {Object} Setter of an object that contains:  image_id, title, artist, description, medium, dimensions
 * @returns {JSX.Element} JSX element representing the Sidebar component.
 */
function Sidebar ({setCurrentObject}) {
     // State hooks for managing user input, error message, search results, and selected list element.
    const [input, setInput]     = useState ('');
    const [error, setError]     = useState (null);
    const [objects, setObjects] = useState (null);
    const [currentList, setCurrentList] = useState(null);
    /**
     * Handles form submission, fetches list of data from the Art Institute of Chicago API.
     * @param {Event} Submit event when form's button clicked 
     */
    function handleSubmit (e) {
        e.preventDefault();
        fetch (`https://api.artic.edu/api/v1/artworks/search?q=${encodeURIComponent(input)}`)
            .then ((response) => {return response.json()})
            .then ((data)     => {setObjects(data);})
            .catch((response) => {setError(response)});
    }
    /**
     * Handles click on a list item, fetches detailed data for the selected object, and updates class selected accordingly.
     * @param {Event} Click event on the list item.
     * @param {Object} Selected object from the list.
     */
    function handleClick (e, object) {
        e.preventDefault();
        fetch (`https://api.artic.edu/api/v1/artworks/${object.id}/?fields=image_id,title,artist_display,description,medium_display,dimensions`)
        .then ((response)    => {return response.json()})
        .then ((imageObject) => {
            setCurrentObject(imageObject); 
            {currentList && currentList.classList.remove('selected')};
            e.target.classList.add('selected');
            setCurrentList(e.target);})
        .catch((response)    => {setError(response)});
    }
    return (<header> 
        <h1>Art Institute of Chicago Search</h1>
        <form onSubmit={(e)=> {handleSubmit(e)}}>
        <div>
            <input onChange={(e) => {setInput(e.target.value);}} value={input} type="text"/>
        </div>
        <button type="submit">Submit</button>
        </form>
        <ul>
            {error   && console.log("Error: " + error)}
            {objects && objects.data.map ((object) => {
                return <li onClick={(e) => { handleClick(e, object) }}>{object.title}</li>
            })}
        </ul>
        </header>)
}
/**
 * Main functional component for displaying details of the selected object.
 * @component
 * @param {Object} Selected object by user that contains:  image_id, title, artist, description, medium, dimensions
 * @returns {JSX.Element} JSX element representing the Main component.
 */
function Main ({currentObject}) {
    if (currentObject != null) {
        return <main>
            <h2>{currentObject.data.title}</h2>
            <h3>{currentObject.data.artist_display}</h3>
            <img src={`https://www.artic.edu/iiif/2/${currentObject.data.image_id}/full/843,/0/default.jpg`} alt="Agressive image!" />
            <div dangerouslySetInnerHTML= { {__html: currentObject.data.description} }/>
            <p> Medium: <br/> {currentObject.data.medium_display}</p>
            <p> Dimension:  <br/>{currentObject.data.dimensions}</p>
        </main>
    }
    else {
        return <main> <h2>Friendly message!</h2> </main>
    }  
}
/**
 * App functional component, the root component rendering Sidebar and Main components.
 * @component
 * @returns {JSX.Element} JSX element representing the App component.
 */
function App() {
    const [currentObject, setCurrentObject] = useState(null);
    return  <div className="wrapper">
                <Sidebar setCurrentObject={setCurrentObject}/>
                <Main currentObject={currentObject}/>
            </div>
}
// Renders App component to the root element
ReactDOM.render(<App/>, document.querySelector('#root'));